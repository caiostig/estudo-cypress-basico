describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"))


    it("Preencher todos os campos de texto", () => {
        const firstname = "Caio"
        const lastname = "Stig"

        cy.get("#first-name").type(firstname);
        cy.get("#last-name").type(lastname);
        cy.get("#email").type("caio@gmail.com");
        cy.get("#requests").type("beer");
        cy.get("#signature").type(`${firstname} ${lastname}`);
    });

    it("Selecionar o segundo ticket", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("Selecionar o tipo do ticket VIP", () => {
        cy.get("#vip").check(); //selecionar radio button
    });

    it("Selecionar o checkbox 'Social Media'", () => {
        cy.get("#social-media").check();
    });

    it("Selecionar 'friend' e 'publication', e depois tirar 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check(); //selecionar checkbox
        cy.get("#friend").uncheck();
    });

    it("O Header tem seu titulo com 'TICKETBOX'", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("Alertar e-mail invalido", () => {
        cy.get("#email").type("caio-gmail.com");
        cy.get("#email.invalid").should("exist");
    });

    it("Alertar e-mail invalido e incluir um valido em seguida", () => {
        cy.get("#email")
          .as("email")
          .type("caio-gmail.com");

        cy.get("#email.invalid").should("exist");
          
        cy.get("@email")
          .clear()
          .type("caio@gmail.com");
        
        cy.get("#email.invalid").should("not.exist");
    });

    it("Preencher e resetar os campos", () => {
        const firstname = "Caio"
        const lastname = "Stig"
        const fullname = `${firstname} ${lastname}`;

        cy.get("#first-name").type(firstname);
        cy.get("#last-name").type(lastname);
        cy.get("#email").type("caio@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("beer");

        cy.get(".agreement p").should(
          "contain",
          `I, ${fullname}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click(); //também funciona para checkbox o click
        cy.get("#signature").type(fullname);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("Preencher campos e ver se o botao esta habilitado ou desabilitado 'usando comandos customizados'", () => {
        const cliente = { //colocar os 3 dados em um objeto.
            firstName: "Joao",
            lastName: "Silva",
            email: "joaosilva@exemplo.com"
        };


        cy.preencherCamposObrigatorios(cliente);


        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");


    });
});